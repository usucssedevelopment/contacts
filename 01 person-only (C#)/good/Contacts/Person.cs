﻿using System.Collections.Generic;
using System.Text;

namespace Contacts
{
    public class Person
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }

        private readonly List<string> _phoneNumbers = new List<string>();

        public string FullName
        {
            get
            {
                if (LastName == null && FirstName == null) return null;
                if (LastName == null) return FirstName;
                if (FirstName == null) return LastName;
                return $"{LastName}, {FirstName}";
            }   
        }
        public string[] PhoneNumbers => _phoneNumbers.ToArray();

        public void AddPhone(string phone)
        {
            if (!_phoneNumbers.Contains(phone))
                _phoneNumbers.Add(phone);
        }

        public void RemovePhone(string phone)
        {
            _phoneNumbers.Remove(phone);
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append(FullName);
            if (_phoneNumbers.Count <= 0)
                return builder.ToString();

            builder.Append(" (");
            builder.Append(string.Join(", ", PhoneNumbers));
            builder.Append(")");
            return builder.ToString();
        }
    }

}
