import org.junit.Test;

import static org.junit.Assert.*;

public class PersonTest {
    
    @Test
    public void testEverything() {
        Person person = new Person();
        assertNull(person.getFirst());
        assertNull(person.getName2());
        assertNull(person.getFullName());
        assertNotNull(person.getNbrs());
        assertEquals(0, person.getNbrs().length);

        person = new Person("Joe", "Jones" );
        assertEquals("Joe", person.getFirst());
        assertEquals("Jones", person.getName2());
        assertEquals("Jones, Joe", person.getFullName());
        assertEquals("Jones, Joe", person.toString());
        assertNotNull(person.getNbrs());
        assertEquals(0, person.getNbrs().length);

        person.setFirst(null);
        assertNull(person.getFirst());
        assertEquals("Jones", person.getName2());
        assertEquals("Jones", person.getFullName());
        assertEquals("Jones", person.toString());

        person.setFirst("Mary");
        person.setName2(null);
        assertEquals("Mary", person.getFirst());
        assertNull(person.getName2());
        assertEquals("Mary", person.getFullName());
        assertEquals("Mary", person.toString());

        person.setName2("Smith");
        assertEquals("Mary", person.getFirst());
        assertEquals("Smith", person.getName2());
        assertEquals("Smith, Mary", person.getFullName());
        assertEquals("Smith, Mary", person.toString());
        assertEquals(0, person.getNbrs().length);

        person.add("235-223-2352");
        assertEquals("Smith, Mary", person.getFullName());
        assertEquals(1, person.getNbrs().length);
        assertEquals("235-223-2352", person.getNbrs()[0]);
        assertEquals("Smith, Mary (235-223-2352)", person.toString());

        person.add("255-235-6333");
        assertEquals(2, person.getNbrs().length);
        assertEquals("235-223-2352", person.getNbrs()[0]);
        assertEquals("255-235-6333", person.getNbrs()[1]);
        assertEquals("Smith, Mary (235-223-2352, 255-235-6333)", person.toString());

        person.add("255-235-6310");
        assertEquals(3, person.getNbrs().length);
        assertEquals("235-223-2352", person.getNbrs()[0]);
        assertEquals("255-235-6333", person.getNbrs()[1]);
        assertEquals("255-235-6310", person.getNbrs()[2]);
        assertEquals("Smith, Mary (235-223-2352, 255-235-6333, 255-235-6310)", person.toString());

        person.remove("255-235-6333");
        assertEquals(2, person.getNbrs().length);
        assertEquals("235-223-2352", person.getNbrs()[0]);
        assertEquals("255-235-6310", person.getNbrs()[1]);
        assertEquals("Smith, Mary (235-223-2352, 255-235-6310)", person.toString());

        person.remove("255-235-6310");
        assertEquals(1, person.getNbrs().length);
        assertEquals("235-223-2352", person.getNbrs()[0]);
        assertEquals("Smith, Mary (235-223-2352)", person.toString());

        person.remove("255-235-bad");
        assertEquals(1, person.getNbrs().length);
        assertEquals("235-223-2352", person.getNbrs()[0]);
        assertEquals("Smith, Mary (235-223-2352)", person.toString());

        person.remove("235-223-2352");
        assertEquals(0, person.getNbrs().length);
        assertEquals("Smith, Mary", person.toString());
    }

}