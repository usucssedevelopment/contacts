import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class Person {

    private String firstName;
    private String lastName;
    private List<String> phoneNumbers = new ArrayList<>();

    public Person() {}
    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }
    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }
    public String getFullName()
    {
        if (lastName == null && firstName == null) return null;
        if (lastName == null) return firstName;
        if (firstName == null) return lastName;
        return String.format("%s, %s",  lastName, firstName);
    }

    public String[] getPhoneNumbers() {
        String[] results = new String[phoneNumbers.size()];
        if (phoneNumbers.size()>0)
            results = phoneNumbers.toArray(results);
        return results;
    }

    public void addPhone(String phone) {
        if (!phoneNumbers.contains(phone))
            phoneNumbers.add(phone);
    }

    public void removePhone(String phone) {
        phoneNumbers.remove(phone);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getFullName());
        if (phoneNumbers.size()==0)
            return builder.toString();

        builder.append(" (");
        builder.append(String.join(", ", phoneNumbers));
        builder.append(")");
        return builder.toString();
    }

}
