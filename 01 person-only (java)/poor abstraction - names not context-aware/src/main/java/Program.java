public class Program {
    public static void main(String[] args) {

        Person person = new Person("Joe", "Jones");
        person.addPersonPhone("235-223-2352");
        person.addPersonPhone("255-235-6333");
        System.out.println(person);

        person.removePersonPhone("255-235-6333");
        System.out.println(person);

        person.removePersonPhone("bad number");
        System.out.println(person);
    }

}
