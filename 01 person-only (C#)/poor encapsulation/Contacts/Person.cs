﻿using System.Collections.Generic;
using System.Text;

namespace Contacts
{
    public class Person
    {

        public string FirstName;
        public string LastName;
        public List<string> PhoneNumbers = new List<string>();

        public string FullName
        {
            get
            {
                if (LastName == null && FirstName == null) return null;
                if (LastName == null) return FirstName;
                if (FirstName == null) return LastName;
                return $"{LastName}, {FirstName}";
            }   
        }

        public void AddPhone(string phone)
        {
            if (!PhoneNumbers.Contains(phone))
                PhoneNumbers.Add(phone);
        }

        public void RemovePhone(string phone)
        {
            PhoneNumbers.Remove(phone);
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append(FullName);
            if (PhoneNumbers.Count <= 0)
                return builder.ToString();

            builder.Append(" (");
            builder.Append(string.Join(", ", PhoneNumbers));
            builder.Append(")");
            return builder.ToString();
        }
    }

}
