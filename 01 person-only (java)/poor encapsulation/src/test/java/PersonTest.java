import org.junit.Test;

import static org.junit.Assert.*;

public class PersonTest {
    
    @Test
    public void testEverything() {
        Person person = new Person();
        assertNull(person.getFirstName());
        assertNull(person.getLastName());
        assertNull(person.getFullName());
        assertNotNull(person.getPhoneNumbers());
        assertEquals(0, person.getPhoneNumbers().size());

        person = new Person("Joe", "Jones" );
        assertEquals("Joe", person.getFirstName());
        assertEquals("Jones", person.getLastName());
        assertEquals("Jones, Joe", person.getFullName());
        assertEquals("Jones, Joe", person.toString());
        assertNotNull(person.getPhoneNumbers());
        assertEquals(0, person.getPhoneNumbers().size());

        person.setFirstName(null);
        assertNull(person.getFirstName());
        assertEquals("Jones", person.getLastName());
        assertEquals("Jones", person.getFullName());
        assertEquals("Jones", person.toString());

        person.setFirstName("Mary");
        person.setLastName(null);
        assertEquals("Mary", person.getFirstName());
        assertNull(person.getLastName());
        assertEquals("Mary", person.getFullName());
        assertEquals("Mary", person.toString());

        person.setLastName("Smith");
        assertEquals("Mary", person.getFirstName());
        assertEquals("Smith", person.getLastName());
        assertEquals("Smith, Mary", person.getFullName());
        assertEquals("Smith, Mary", person.toString());
        assertEquals(0, person.getPhoneNumbers().size());

        person.addPhone("235-223-2352");
        assertEquals("Smith, Mary", person.getFullName());
        assertEquals(1, person.getPhoneNumbers().size());
        assertEquals("235-223-2352", person.getPhoneNumbers().get(0));
        assertEquals("Smith, Mary (235-223-2352)", person.toString());

        person.addPhone("255-235-6333");
        assertEquals(2, person.getPhoneNumbers().size());
        assertEquals("235-223-2352", person.getPhoneNumbers().get(0));
        assertEquals("255-235-6333", person.getPhoneNumbers().get(1));
        assertEquals("Smith, Mary (235-223-2352, 255-235-6333)", person.toString());

        person.addPhone("255-235-6310");
        assertEquals(3, person.getPhoneNumbers().size());
        assertEquals("235-223-2352", person.getPhoneNumbers().get(0));
        assertEquals("255-235-6333", person.getPhoneNumbers().get(1));
        assertEquals("255-235-6310", person.getPhoneNumbers().get(2));
        assertEquals("Smith, Mary (235-223-2352, 255-235-6333, 255-235-6310)", person.toString());

        person.removePhone("255-235-6333");
        assertEquals(2, person.getPhoneNumbers().size());
        assertEquals("235-223-2352", person.getPhoneNumbers().get(0));
        assertEquals("255-235-6310", person.getPhoneNumbers().get(1));
        assertEquals("Smith, Mary (235-223-2352, 255-235-6310)", person.toString());

        person.removePhone("255-235-6310");
        assertEquals(1, person.getPhoneNumbers().size());
        assertEquals("235-223-2352", person.getPhoneNumbers().get(0));
        assertEquals("Smith, Mary (235-223-2352)", person.toString());

        person.removePhone("255-235-bad");
        assertEquals(1, person.getPhoneNumbers().size());
        assertEquals("235-223-2352", person.getPhoneNumbers().get(0));
        assertEquals("Smith, Mary (235-223-2352)", person.toString());

        person.removePhone("235-223-2352");
        assertEquals(0, person.getPhoneNumbers().size());
        assertEquals("Smith, Mary", person.toString());
    }

}