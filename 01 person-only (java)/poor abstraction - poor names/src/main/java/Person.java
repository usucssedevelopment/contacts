import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class Person {

    private String first;
    private String name2;
    private List<String> nbrs = new ArrayList<>();

    public Person() {}
    public Person(String first, String name2) {
        this.first = first;
        this.name2 = name2;
    }

    public String getFirst() { return first; }
    public void setFirst(String first) { this.first = first; }
    public String getName2() { return name2; }
    public void setName2(String name2) { this.name2 = name2; }
    public String getFullName()
    {
        if (name2 == null && first == null) return null;
        if (name2 == null) return first;
        if (first == null) return name2;
        return String.format("%s, %s", name2, first);
    }

    public String[] getNbrs() {
        String[] results = new String[nbrs.size()];
        if (nbrs.size()>0)
            results = nbrs.toArray(results);
        return results;
    }

    public void add(String phone) {
        if (!nbrs.contains(phone))
            nbrs.add(phone);
    }

    public void remove(String phone) {
        nbrs.remove(phone);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getFullName());
        if (nbrs.size()==0)
            return builder.toString();

        builder.append(" (");
        builder.append(String.join(", ", nbrs));
        builder.append(")");
        return builder.toString();
    }

}
