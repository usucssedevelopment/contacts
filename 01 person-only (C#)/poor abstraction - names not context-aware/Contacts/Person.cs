﻿using System.Collections.Generic;
using System.Text;

namespace Contacts
{
    public class Person
    {

        public string PersonFirstName { get; set; }
        public string PersonLastName { get; set; }

        private readonly List<string> _personPhoneNumbers = new List<string>();

        public string PersonFullName
        {
            get
            {
                if (PersonLastName == null && PersonFirstName == null) return null;
                if (PersonLastName == null) return PersonFirstName;
                if (PersonFirstName == null) return PersonLastName;
                return $"{PersonLastName}, {PersonFirstName}";
            }   
        }
        public string[] PersonPhoneNumbers => _personPhoneNumbers.ToArray();

        public void AddPersonPhone(string phone)
        {
            if (!_personPhoneNumbers.Contains(phone))
                _personPhoneNumbers.Add(phone);
        }

        public void RemovePersonPhone(string phone)
        {
            _personPhoneNumbers.Remove(phone);
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append(PersonFullName);
            if (_personPhoneNumbers.Count <= 0)
                return builder.ToString();

            builder.Append(" (");
            builder.Append(string.Join(", ", PersonPhoneNumbers));
            builder.Append(")");
            return builder.ToString();
        }
    }

}
