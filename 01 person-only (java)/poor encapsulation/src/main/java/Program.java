public class Program {
    public static void main(String[] args) {

        Person person = new Person("Joe", "Jones");
        person.addPhone("235-223-2352");
        person.addPhone("255-235-6333");
        System.out.println(person);

        person.removePhone("255-235-6333");
        System.out.println(person);

        person.removePhone("bad number");
        System.out.println(person);
    }

}
