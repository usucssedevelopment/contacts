﻿using Contacts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ContactsTest
{
    [TestClass]
    public class PersonTest
    {
        [TestMethod]
        public void Contacts_TestEverything()
        {
            var person = new Person();
            Assert.IsNull(person.FirstName);
            Assert.IsNull(person.LastName);
            Assert.IsNull(person.FullName);
            Assert.IsNotNull(person.PhoneNumbers);
            Assert.AreEqual(0, person.PhoneNumbers.Length);

            person = new Person() { FirstName = "Joe", LastName = "Jones" };
            Assert.AreEqual("Joe", person.FirstName);
            Assert.AreEqual("Jones", person.LastName);
            Assert.AreEqual("Jones, Joe", person.FullName);
            Assert.AreEqual("Jones, Joe", person.ToString());
            Assert.IsNotNull(person.PhoneNumbers);
            Assert.AreEqual(0, person.PhoneNumbers.Length);

            person.FirstName = null;
            Assert.IsNull(person.FirstName);
            Assert.AreEqual("Jones", person.LastName);
            Assert.AreEqual("Jones", person.FullName);
            Assert.AreEqual("Jones", person.ToString());

            person.FirstName = "Mary";
            person.LastName = null;
            Assert.AreEqual("Mary", person.FirstName);
            Assert.IsNull(person.LastName);
            Assert.AreEqual("Mary", person.FullName);
            Assert.AreEqual("Mary", person.ToString());

            person.LastName = "Smith";
            Assert.AreEqual("Mary", person.FirstName);
            Assert.AreEqual("Smith", person.LastName);
            Assert.AreEqual("Smith, Mary", person.FullName);
            Assert.AreEqual("Smith, Mary", person.ToString());
            Assert.AreEqual(0, person.PhoneNumbers.Length);

            person.AddPhone("235-223-2352");
            Assert.AreEqual("Smith, Mary", person.FullName);
            Assert.AreEqual(1, person.PhoneNumbers.Length);
            Assert.AreEqual("235-223-2352", person.PhoneNumbers[0]);
            Assert.AreEqual("Smith, Mary (235-223-2352)", person.ToString());

            person.AddPhone("255-235-6333");
            Assert.AreEqual(2, person.PhoneNumbers.Length);
            Assert.AreEqual("235-223-2352", person.PhoneNumbers[0]);
            Assert.AreEqual("255-235-6333", person.PhoneNumbers[1]);
            Assert.AreEqual("Smith, Mary (235-223-2352, 255-235-6333)", person.ToString());

            person.AddPhone("255-235-6310");
            Assert.AreEqual(3, person.PhoneNumbers.Length);
            Assert.AreEqual("235-223-2352", person.PhoneNumbers[0]);
            Assert.AreEqual("255-235-6333", person.PhoneNumbers[1]);
            Assert.AreEqual("255-235-6310", person.PhoneNumbers[2]);
            Assert.AreEqual("Smith, Mary (235-223-2352, 255-235-6333, 255-235-6310)", person.ToString());

            person.RemovePhone("255-235-6333");
            Assert.AreEqual(2, person.PhoneNumbers.Length);
            Assert.AreEqual("235-223-2352", person.PhoneNumbers[0]);
            Assert.AreEqual("255-235-6310", person.PhoneNumbers[1]);
            Assert.AreEqual("Smith, Mary (235-223-2352, 255-235-6310)", person.ToString());

            person.RemovePhone("255-235-6310");
            Assert.AreEqual(1, person.PhoneNumbers.Length);
            Assert.AreEqual("235-223-2352", person.PhoneNumbers[0]);
            Assert.AreEqual("Smith, Mary (235-223-2352)", person.ToString());

            person.RemovePhone("255-235-bad");
            Assert.AreEqual(1, person.PhoneNumbers.Length);
            Assert.AreEqual("235-223-2352", person.PhoneNumbers[0]);
            Assert.AreEqual("Smith, Mary (235-223-2352)", person.ToString());

            person.RemovePhone("235-223-2352");
            Assert.AreEqual(0, person.PhoneNumbers.Length);
            Assert.AreEqual("Smith, Mary", person.ToString());
        }
    }
}
