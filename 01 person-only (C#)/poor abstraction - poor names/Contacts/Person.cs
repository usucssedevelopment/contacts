﻿using System.Collections.Generic;
using System.Text;

namespace Contacts
{
    public class Person
    {

        public string First { get; set; }
        public string Name2 { get; set; }

        private readonly List<string> _nbrs = new List<string>();

        public string FullName
        {
            get
            {
                if (Name2 == null && First == null) return null;
                if (Name2 == null) return First;
                if (First == null) return Name2;
                return $"{Name2}, {First}";
            }   
        }
        public string[] Nbrs => _nbrs.ToArray();

        public void Add(string phone)
        {
            if (!_nbrs.Contains(phone))
                _nbrs.Add(phone);
        }

        public void Remove(string phone)
        {
            _nbrs.Remove(phone);
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append(FullName);
            if (_nbrs.Count <= 0)
                return builder.ToString();

            builder.Append(" (");
            builder.Append(string.Join(", ", Nbrs));
            builder.Append(")");
            return builder.ToString();
        }
    }

}
