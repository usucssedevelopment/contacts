public class Program {
    public static void main(String[] args) {

        Person person = new Person("Joe", "Jones");
        person.add("235-223-2352");
        person.add("255-235-6333");
        System.out.println(person);

        person.remove("255-235-6333");
        System.out.println(person);

        person.remove("bad number");
        System.out.println(person);
    }

}
