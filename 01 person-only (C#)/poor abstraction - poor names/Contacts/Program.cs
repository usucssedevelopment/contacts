﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var person = new Person() {First = "Joe", Name2 = "Jones"};
            person.Add("235-223-2352");
            person.Add("255-235-6333");
            Console.WriteLine(person);

            person.Remove("255-235-6333");
            Console.WriteLine(person);

            person.Remove("bad number");
            Console.WriteLine(person);

            Console.WriteLine("Type any key to continue");
            Console.ReadKey();
        }
    }
}
