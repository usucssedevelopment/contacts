﻿using Contacts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ContactsTest
{
    [TestClass]
    public class PersonTest
    {
        [TestMethod]
        public void Contacts_TestEverything()
        {
            var person = new Person();
            Assert.IsNull(person.First);
            Assert.IsNull(person.Name2);
            Assert.IsNull(person.FullName);
            Assert.IsNotNull(person.Nbrs);
            Assert.AreEqual(0, person.Nbrs.Length);

            person = new Person() { First = "Joe", Name2 = "Jones" };
            Assert.AreEqual("Joe", person.First);
            Assert.AreEqual("Jones", person.Name2);
            Assert.AreEqual("Jones, Joe", person.FullName);
            Assert.AreEqual("Jones, Joe", person.ToString());
            Assert.IsNotNull(person.Nbrs);
            Assert.AreEqual(0, person.Nbrs.Length);

            person.First = null;
            Assert.IsNull(person.First);
            Assert.AreEqual("Jones", person.Name2);
            Assert.AreEqual("Jones", person.FullName);
            Assert.AreEqual("Jones", person.ToString());

            person.First = "Mary";
            person.Name2 = null;
            Assert.AreEqual("Mary", person.First);
            Assert.IsNull(person.Name2);
            Assert.AreEqual("Mary", person.FullName);
            Assert.AreEqual("Mary", person.ToString());

            person.Name2 = "Smith";
            Assert.AreEqual("Mary", person.First);
            Assert.AreEqual("Smith", person.Name2);
            Assert.AreEqual("Smith, Mary", person.FullName);
            Assert.AreEqual("Smith, Mary", person.ToString());
            Assert.AreEqual(0, person.Nbrs.Length);

            person.Add("235-223-2352");
            Assert.AreEqual("Smith, Mary", person.FullName);
            Assert.AreEqual(1, person.Nbrs.Length);
            Assert.AreEqual("235-223-2352", person.Nbrs[0]);
            Assert.AreEqual("Smith, Mary (235-223-2352)", person.ToString());

            person.Add("255-235-6333");
            Assert.AreEqual(2, person.Nbrs.Length);
            Assert.AreEqual("235-223-2352", person.Nbrs[0]);
            Assert.AreEqual("255-235-6333", person.Nbrs[1]);
            Assert.AreEqual("Smith, Mary (235-223-2352, 255-235-6333)", person.ToString());

            person.Add("255-235-6310");
            Assert.AreEqual(3, person.Nbrs.Length);
            Assert.AreEqual("235-223-2352", person.Nbrs[0]);
            Assert.AreEqual("255-235-6333", person.Nbrs[1]);
            Assert.AreEqual("255-235-6310", person.Nbrs[2]);
            Assert.AreEqual("Smith, Mary (235-223-2352, 255-235-6333, 255-235-6310)", person.ToString());

            person.Remove("255-235-6333");
            Assert.AreEqual(2, person.Nbrs.Length);
            Assert.AreEqual("235-223-2352", person.Nbrs[0]);
            Assert.AreEqual("255-235-6310", person.Nbrs[1]);
            Assert.AreEqual("Smith, Mary (235-223-2352, 255-235-6310)", person.ToString());

            person.Remove("255-235-6310");
            Assert.AreEqual(1, person.Nbrs.Length);
            Assert.AreEqual("235-223-2352", person.Nbrs[0]);
            Assert.AreEqual("Smith, Mary (235-223-2352)", person.ToString());

            person.Remove("255-235-bad");
            Assert.AreEqual(1, person.Nbrs.Length);
            Assert.AreEqual("235-223-2352", person.Nbrs[0]);
            Assert.AreEqual("Smith, Mary (235-223-2352)", person.ToString());

            person.Remove("235-223-2352");
            Assert.AreEqual(0, person.Nbrs.Length);
            Assert.AreEqual("Smith, Mary", person.ToString());
        }
    }
}
