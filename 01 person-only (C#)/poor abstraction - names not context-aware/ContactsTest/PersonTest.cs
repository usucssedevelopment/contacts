﻿using Contacts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ContactsTest
{
    [TestClass]
    public class PersonTest
    {
        [TestMethod]
        public void Contacts_TestEverything()
        {
            var person = new Person();
            Assert.IsNull(person.PersonFirstName);
            Assert.IsNull(person.PersonLastName);
            Assert.IsNull(person.PersonFullName);
            Assert.IsNotNull(person.PersonPhoneNumbers);
            Assert.AreEqual(0, person.PersonPhoneNumbers.Length);

            person = new Person() { PersonFirstName = "Joe", PersonLastName = "Jones" };
            Assert.AreEqual("Joe", person.PersonFirstName);
            Assert.AreEqual("Jones", person.PersonLastName);
            Assert.AreEqual("Jones, Joe", person.PersonFullName);
            Assert.AreEqual("Jones, Joe", person.ToString());
            Assert.IsNotNull(person.PersonPhoneNumbers);
            Assert.AreEqual(0, person.PersonPhoneNumbers.Length);

            person.PersonFirstName = null;
            Assert.IsNull(person.PersonFirstName);
            Assert.AreEqual("Jones", person.PersonLastName);
            Assert.AreEqual("Jones", person.PersonFullName);
            Assert.AreEqual("Jones", person.ToString());

            person.PersonFirstName = "Mary";
            person.PersonLastName = null;
            Assert.AreEqual("Mary", person.PersonFirstName);
            Assert.IsNull(person.PersonLastName);
            Assert.AreEqual("Mary", person.PersonFullName);
            Assert.AreEqual("Mary", person.ToString());

            person.PersonLastName = "Smith";
            Assert.AreEqual("Mary", person.PersonFirstName);
            Assert.AreEqual("Smith", person.PersonLastName);
            Assert.AreEqual("Smith, Mary", person.PersonFullName);
            Assert.AreEqual("Smith, Mary", person.ToString());
            Assert.AreEqual(0, person.PersonPhoneNumbers.Length);

            person.AddPersonPhone("235-223-2352");
            Assert.AreEqual("Smith, Mary", person.PersonFullName);
            Assert.AreEqual(1, person.PersonPhoneNumbers.Length);
            Assert.AreEqual("235-223-2352", person.PersonPhoneNumbers[0]);
            Assert.AreEqual("Smith, Mary (235-223-2352)", person.ToString());

            person.AddPersonPhone("255-235-6333");
            Assert.AreEqual(2, person.PersonPhoneNumbers.Length);
            Assert.AreEqual("235-223-2352", person.PersonPhoneNumbers[0]);
            Assert.AreEqual("255-235-6333", person.PersonPhoneNumbers[1]);
            Assert.AreEqual("Smith, Mary (235-223-2352, 255-235-6333)", person.ToString());

            person.AddPersonPhone("255-235-6310");
            Assert.AreEqual(3, person.PersonPhoneNumbers.Length);
            Assert.AreEqual("235-223-2352", person.PersonPhoneNumbers[0]);
            Assert.AreEqual("255-235-6333", person.PersonPhoneNumbers[1]);
            Assert.AreEqual("255-235-6310", person.PersonPhoneNumbers[2]);
            Assert.AreEqual("Smith, Mary (235-223-2352, 255-235-6333, 255-235-6310)", person.ToString());

            person.RemovePersonPhone("255-235-6333");
            Assert.AreEqual(2, person.PersonPhoneNumbers.Length);
            Assert.AreEqual("235-223-2352", person.PersonPhoneNumbers[0]);
            Assert.AreEqual("255-235-6310", person.PersonPhoneNumbers[1]);
            Assert.AreEqual("Smith, Mary (235-223-2352, 255-235-6310)", person.ToString());

            person.RemovePersonPhone("255-235-6310");
            Assert.AreEqual(1, person.PersonPhoneNumbers.Length);
            Assert.AreEqual("235-223-2352", person.PersonPhoneNumbers[0]);
            Assert.AreEqual("Smith, Mary (235-223-2352)", person.ToString());

            person.RemovePersonPhone("255-235-bad");
            Assert.AreEqual(1, person.PersonPhoneNumbers.Length);
            Assert.AreEqual("235-223-2352", person.PersonPhoneNumbers[0]);
            Assert.AreEqual("Smith, Mary (235-223-2352)", person.ToString());

            person.RemovePersonPhone("235-223-2352");
            Assert.AreEqual(0, person.PersonPhoneNumbers.Length);
            Assert.AreEqual("Smith, Mary", person.ToString());
        }
    }
}
