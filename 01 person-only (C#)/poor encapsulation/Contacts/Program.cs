﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var person = new Person() {FirstName = "Joe", LastName = "Jones"};
            person.AddPhone("235-223-2352");
            person.AddPhone("255-235-6333");
            Console.WriteLine(person);

            person.RemovePhone("255-235-6333");
            Console.WriteLine(person);

            person.RemovePhone("bad number");
            Console.WriteLine(person);

            Console.WriteLine("Type any key to continue");
            Console.ReadKey();
        }
    }
}
