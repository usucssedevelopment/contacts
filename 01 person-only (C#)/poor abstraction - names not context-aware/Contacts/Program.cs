﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var person = new Person() {PersonFirstName = "Joe", PersonLastName = "Jones"};
            person.AddPersonPhone("235-223-2352");
            person.AddPersonPhone("255-235-6333");
            Console.WriteLine(person);

            person.RemovePersonPhone("255-235-6333");
            Console.WriteLine(person);

            person.RemovePersonPhone("bad number");
            Console.WriteLine(person);

            Console.WriteLine("Type any key to continue");
            Console.ReadKey();
        }
    }
}
