import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class Person {

    private String personFirstName;
    private String personLastName;
    private List<String> personPhoneNumbers = new ArrayList<>();

    public Person() {}
    public Person(String personFirstName, String personLastName) {
        this.personFirstName = personFirstName;
        this.personLastName = personLastName;
    }

    public String getPersonFirstName() { return personFirstName; }
    public void setPersonFirstName(String personFirstName) { this.personFirstName = personFirstName; }
    public String getPersonLastName() { return personLastName; }
    public void setPersonLastName(String personLastName) { this.personLastName = personLastName; }
    public String getFullName()
    {
        if (personLastName == null && personFirstName == null) return null;
        if (personLastName == null) return personFirstName;
        if (personFirstName == null) return personLastName;
        return String.format("%s, %s", personLastName, personFirstName);
    }

    public String[] getPersonPhoneNumbers() {
        String[] results = new String[personPhoneNumbers.size()];
        if (personPhoneNumbers.size()>0)
            results = personPhoneNumbers.toArray(results);
        return results;
    }

    public void addPersonPhone(String phone) {
        if (!personPhoneNumbers.contains(phone))
            personPhoneNumbers.add(phone);
    }

    public void removePersonPhone(String phone) {
        personPhoneNumbers.remove(phone);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getFullName());
        if (personPhoneNumbers.size()==0)
            return builder.toString();

        builder.append(" (");
        builder.append(String.join(", ", personPhoneNumbers));
        builder.append(")");
        return builder.toString();
    }

}
