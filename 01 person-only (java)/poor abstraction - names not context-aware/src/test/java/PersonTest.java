import org.junit.Test;

import static org.junit.Assert.*;

public class PersonTest {
    
    @Test
    public void testEverything() {
        Person person = new Person();
        assertNull(person.getPersonFirstName());
        assertNull(person.getPersonLastName());
        assertNull(person.getFullName());
        assertNotNull(person.getPersonPhoneNumbers());
        assertEquals(0, person.getPersonPhoneNumbers().length);

        person = new Person("Joe", "Jones" );
        assertEquals("Joe", person.getPersonFirstName());
        assertEquals("Jones", person.getPersonLastName());
        assertEquals("Jones, Joe", person.getFullName());
        assertEquals("Jones, Joe", person.toString());
        assertNotNull(person.getPersonPhoneNumbers());
        assertEquals(0, person.getPersonPhoneNumbers().length);

        person.setPersonFirstName(null);
        assertNull(person.getPersonFirstName());
        assertEquals("Jones", person.getPersonLastName());
        assertEquals("Jones", person.getFullName());
        assertEquals("Jones", person.toString());

        person.setPersonFirstName("Mary");
        person.setPersonLastName(null);
        assertEquals("Mary", person.getPersonFirstName());
        assertNull(person.getPersonLastName());
        assertEquals("Mary", person.getFullName());
        assertEquals("Mary", person.toString());

        person.setPersonLastName("Smith");
        assertEquals("Mary", person.getPersonFirstName());
        assertEquals("Smith", person.getPersonLastName());
        assertEquals("Smith, Mary", person.getFullName());
        assertEquals("Smith, Mary", person.toString());
        assertEquals(0, person.getPersonPhoneNumbers().length);

        person.addPersonPhone("235-223-2352");
        assertEquals("Smith, Mary", person.getFullName());
        assertEquals(1, person.getPersonPhoneNumbers().length);
        assertEquals("235-223-2352", person.getPersonPhoneNumbers()[0]);
        assertEquals("Smith, Mary (235-223-2352)", person.toString());

        person.addPersonPhone("255-235-6333");
        assertEquals(2, person.getPersonPhoneNumbers().length);
        assertEquals("235-223-2352", person.getPersonPhoneNumbers()[0]);
        assertEquals("255-235-6333", person.getPersonPhoneNumbers()[1]);
        assertEquals("Smith, Mary (235-223-2352, 255-235-6333)", person.toString());

        person.addPersonPhone("255-235-6310");
        assertEquals(3, person.getPersonPhoneNumbers().length);
        assertEquals("235-223-2352", person.getPersonPhoneNumbers()[0]);
        assertEquals("255-235-6333", person.getPersonPhoneNumbers()[1]);
        assertEquals("255-235-6310", person.getPersonPhoneNumbers()[2]);
        assertEquals("Smith, Mary (235-223-2352, 255-235-6333, 255-235-6310)", person.toString());

        person.removePersonPhone("255-235-6333");
        assertEquals(2, person.getPersonPhoneNumbers().length);
        assertEquals("235-223-2352", person.getPersonPhoneNumbers()[0]);
        assertEquals("255-235-6310", person.getPersonPhoneNumbers()[1]);
        assertEquals("Smith, Mary (235-223-2352, 255-235-6310)", person.toString());

        person.removePersonPhone("255-235-6310");
        assertEquals(1, person.getPersonPhoneNumbers().length);
        assertEquals("235-223-2352", person.getPersonPhoneNumbers()[0]);
        assertEquals("Smith, Mary (235-223-2352)", person.toString());

        person.removePersonPhone("255-235-bad");
        assertEquals(1, person.getPersonPhoneNumbers().length);
        assertEquals("235-223-2352", person.getPersonPhoneNumbers()[0]);
        assertEquals("Smith, Mary (235-223-2352)", person.toString());

        person.removePersonPhone("235-223-2352");
        assertEquals(0, person.getPersonPhoneNumbers().length);
        assertEquals("Smith, Mary", person.toString());
    }

}